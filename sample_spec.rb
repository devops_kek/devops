require 'spec_helper'

describe package('mongodb-org'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe package('nodejs'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe service('mongod'), :if => os[:family] == 'ubuntu' do
  it { should be_running }
end

describe port(8884) do
  it { should be_listening }
end
