const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Promise = require('bluebird');

var HistorySchema = new Schema({
  number1: Number,
  number2: Number,
  operator: String
});

History = mongoose.model('History', HistorySchema);

var exportation = {
  saveHistory : function (number1, number2,operator){
    return History({number1, number2, operator}).save();
  },

  getHistory : function(){
    return new Promise(function(resolve,reject){
      History.find({},function(err, item){
        if(err){
          reject(err);
        }
        else {
          resolve(item);
        }
      });
    });
  }


};

module.exports = exportation;
