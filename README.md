# DevOps

Serveurs :

* Mao 35.157.107.80 Dev
* Fidel 35.157.158.241 Prod
* Franco 35.156.136.218 Deployment controller

deployment : ansible-playbook -i hosts deploy.yml --extra-vars "version=V0.1.0 hosts=dev"

##Php webservice
Php webservice running on Franco called by bitbucket pipeline after succesfull push to branch

###/var/www/html/index.php (Franco)
``` php
<?php
$version = $_POST['version'];
$key = $_POST�['key'];
$host = $_POST['hosts'];
var_dump($version);
if($key = "azerty"){
	$output = shell_exec("sudo -u exploit ansible-playbook -i /home/exploit/pipeline/devops/provision/hosts /home/exploit/pipeline/devops/provision/deploy.yml --extra-vars \"version=".$version." hosts=".$host."\"");
}
var_dump($output);
?>
```

###bitbucket-pipelines.yml
``` php
image: node:4.6.0

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - npm install
          - npm test

  custom: # Pipelines that are triggered manually
    deployment-to-prod:
      - step:
          script:
            - curl --data "version=master&key=azerty&hosts=prod" http://35.156.136.218
    deployment-to-dev:
      - step:
          script:
             - curl --data "version=develop&key=azerty&hosts=dev" http://35.156.136.218
  branches:  # Pipelines that run automatically on a commit to a branch
    develop:
      - step:
          script:
            - curl --data "version=develop&key=azerty&hosts=dev" http://35.156.136.218
    master:
      - step:
          script:
            - curl --data "version=master&key=azerty&hosts=prod" http://35.156.136.218

```

##App
###Port : 8884
###Routes :

* /
* math/add/{}/{}
* math/soustract/{}/{}
* math/divide/{}/{}
* diamond

##Packages

* NodeJs 6.9.5
* PM2
* Mongoose

##Monitoring

CollectD and statsD installed on Mao and Fidel, provisioning will start the statsD docker but you will need to have run the docker atleast once before.
```
sudo docker run -d --name statsd -v /etc/statsd:/etc/statsd -p 8125:8125/udp -p 8126:8126 ennexa/statsd
```

Graphite running on Franco http://35.156.136.218:8080/
User : exploit
Pass : grafana

Grafana running collectD on the dev and prod server along with statsD on the app.
http://35.156.136.218:3000/dashboard/db/system-metrics
user : admin
pass: admin

##ServerSpec

Serverspec va nous servir à créer des RSpec afin de regarder si notre serveur est configuré comme on le souhaite.

###Installation

Tout d’abord il faudra installer Ruby car les applications Serverspec sont écrites en Ruby, et « gem » car c’est la librairie Ruby qui contient tous les plugins, extensions et applications que Serverspec a besoin à l’aide de cette commande :

```bash
root@host ~ # apt-get install ruby rubygems bundler
root@host ~ # bundle-init
```

Dans notre fichier Gemfile on ajoute cette ligne dans notre fichier

```bash
root@host ~ # gem ‘serverspec’
```

Compile serverspec

```bash
root@host ~ # bundle
```

On installe serverspec

```bash
root@host ~ # gem install serverspec
```

On créer notre dossier qui contiendra le fichier de configuration des applications/services/processus qu’on souhaitera vérifier sur nos serveurs :

```bash
root@host ~ # serverspec-init
Select OS type:

  1) UN*X
  2) Windows

Select number: 1

Select a backend type:

  1) SSH
  2) Exec (local)

Select number: 1

Vagrant instance y/n: n
Input target host name: ip-de-notre-serveur
 + spec/
 + spec/ip-de-notre-serveur/
 + spec/ip-de-notre-serveur/sample_spec.rb
 + spec/spec_helper.rb
 + Rakefile
 + .rspec
```

Ensuite nous nous rendons dans le dossier que l’on vient de créer pour modifier le fichier sample_spec.rb afin de rentrer les services/applications et processus que l’on souhaite consulter :

```bash
require 'spec_helper'

describe package('mongodb-org'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe package('nodejs'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe service('mongod'), :if => os[:family] == 'ubuntu' do
  it { should be_running }
end

describe port(8884) do
  it { should be_listening }
end
```
