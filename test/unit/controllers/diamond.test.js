describe("Module Diamond", () => {
  describe("GET /diamond", function(request) {
    it('should redirect to /diamond/new', function() {
      return server.run({
        method: 'get',
        url: '/diamond'
      }).then(function(res) {
        res.headers.location.should.equal("/diamond/new");
      })
    })
  })

  describe("GET /diamond/new", function() {
    it('should show a form with an input and a submit', function() {
      return server.run({
        method: 'get',
        url: '/diamond/new'
      }).then(function(res) {
        //TODO
        res.statusCode.should.equal(200);
        res.result.should.contains("<form");
        res.result.should.contains("<input");
        res.result.should.contains('type="submit"');
      })
    })
  })

  describe("POST /diamond", function() {
    it('should reject when `width` parameter is not a number', function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: "coucou" }
      }).then(function(res) {
          res.statusCode.should.equal(400);
      })
    })

    // odd = impair
    it('should reject when `width` parameter is not odd', function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: 2 }
      }).then(function(res) {
          res.statusCode.should.equal(400);
      })
    })


    testDiamonds = [{
      width: 1,
      result: '+'
    }, {
      width: 3,
      result: [
        '.+.',
        '+++',
        '.+.'
      ].join("\n")
    }, {
      width: 5,
      result: [
        '..+..',
        '.+++.',
        '+++++',
        '.+++.',
        '..+..'
      ].join("\n")
    }]

    // C'est possible de créér des tests unitaires de façon dynamique
    testDiamonds.forEach(function(diamond){
      it(`should show correct result on width = ${diamond.width}`, function() {
        return server.run({
          method: 'post',
          url: '/diamond',
          payload: { width: diamond.width }
        }).then(function(res) {
          res.statusCode.should.equal(200);
          res.result.should.equal(diamond.result);
        })
      })
    })

    it('should invert + and . when given `width` is negative', function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: -5 }
      }).then(function(res) {
        var expected = [
          '++.++',
          '+...+',
          '.....',
          '+...+',
          '++.++'
        ].join("\n");
        res.statusCode.should.equal(200);
        res.result.should.equal(expected);
      })
    })
  })
})
