// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');


module.exports.random = function(request,reply){
  var size = request.params.largeur;
  var maxValue = request.params.hauteur;
  var randomValues = [];
  var response = "";

  for(i=0; i<size; i++){
    randomValues[i] = getRandom(0, maxValue);
  }

  randomValues[getRandom(0, randomValues.length)] = maxValue;
  reply(diagram(randomValues));
}

function getRandom(min, max) {
  return Math.floor(Math.random() * (max-min+1)+min);
}

function getArray(values) {

  var indexPile = [];
  var i = 0;
  var maxArea = 0;

  while(i < values.length) {
    if(indexPile.length == 0 || values[indexPile[indexPile.length-1]] <= values[i])
    {
      indexPile.push(i);
      i++;
    } else {
      var current = indexPile.pop();
      var curVal = values[current];
      var area_current = indexPile.length == 0 ? i : i - indexPile[indexPile.length-1] - 1;
      maxArea = maxArea > area_current * curVal ? maxArea : area_current*curVal;
    }
  }

  while (indexPile.length > 0) {
    var current = indexPile.pop();
    var curVal = values[current];
    var area_current = indexPile.length == 0 ? i : i - indexPile[indexPile.length-1] - 1;
    maxArea = maxArea > area_current * curVal ? maxArea : area_current*curVal;
  }

  return maxArea;
}


function diagram(values) {
  var char = "$";
  var matrice = [];
  var html = "";

  var highest = Math.max(...values);
  for(i = 0; i<=highest; i++) {
    matrice[i] = [];
    for(j = 0; j <= values.length; j++) {
      matrice[i][j] = " ";
    }
  }
  matrice[highest][0] = "+";
  for(j = 0; j < values.length; j++) {
    matrice[highest][j+1] = j+1;
  }

  for(k = 0; k < highest; k++) {
    matrice[k][0] = highest-k;
  }

  for(j = 0; j < highest; j++) {
    for(i = 1; i <= values.length; i++) {
      if(values[i-1] >= j+1) {
        matrice[(highest-1)-j][i] = char;
      }
    }
  }

  html = "<table>";
  for(i=0; i<matrice.length; i++) {
    html += "<tr>";
    for(j=0; j<=values.length; j++) {
      if(!isNaN(matrice[i][j])) {
        html += "<td style=\"color:green\">" + matrice[i][j] + "</td>";
      } else {
        html += "<td>" + matrice[i][j] + "</td>";
      }
    }
    html += "</tr>";
  }
  html += "</table>";
  html += "<p> l\'aire maximale est de " + getArray(values) + " carrés</p>";
  // Response
  return html;
}

module.exports.rectangle = function(request, reply){
  reply(diagram(request.params.values.split(',')));
}
