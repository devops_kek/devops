// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
const historyFonction = require('../models/history.js');
var StatsD = require('node-statsd'),
    client = new StatsD();

module.exports.getAdd = function(request, reply){
  // correct any spelling errors made by non english users
  term1 = request.params.term1.replace(",",".");
  term2 = request.params.term2.replace(",",".");

  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(term1)
  term2 = parseFloat(term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }
  client.increment('nb_addition');
  historyFonction.saveHistory(term1, term2, '+');
  reply((term1 + term2).toFixed(2))
}

module.exports.getDivide = function(request, reply){
  dividend = parseFloat(request.params.dividend)
  divisor = parseFloat(request.params.divisor)

  if(isNaN(dividend) || isNaN(divisor)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  if(divisor === 0) {
    return reply(Boom.badRequest('Divisor can not be 0.'))
  }
  historyFonction.saveHistory(dividend, divisor, '/');
  client.increment('nb_division');
  reply(dividend / divisor)
}

module.exports.getMultiplication = function (request, reply) {
    num1 = parseFloat(request.params.num1)
    num2 = parseFloat(request.params.num2)

    if((isNaN(num1) || num1.toString().indexOf('.') != -1) || (isNaN(num2) || num2.toString().indexOf('.') != -1)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }
    historyFonction.saveHistory(num1, num2, '*');
    client.increment('nb_multiplication');
    reply(num1 * num2)
}


module.exports.getSoustrac = function(request, reply){
  number1 = parseFloat(request.params.number1)
  number2 = parseFloat(request.params.number2)

  if((isNaN(number1) || number1.toString().indexOf('.') != -1) || (isNaN(number2) || number2.toString().indexOf('.') != -1)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }
  client.increment('nb_soustraction');
  historyFonction.saveHistory(number1, number2, '-');
  reply(number1 - number2)
}

module.exports.getHistory = function(request, reply){
  historyFonction.getHistory().then((historys) => {
    var history = ""
    historys.forEach(function(operation){
      history += operation.number1 + " " + operation.operator + " " + operation.number2 + "<br>"
    })
    reply(history)
  });
}
