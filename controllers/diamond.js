// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.index = function(request, reply){
  reply.redirect("/diamond/new");
}

module.exports.showForm = function(request, reply){
  reply('<form method="post" action="/diamond"><input type="number" name="width"><br><br><input type="submit" value="Submit"></form>');
}

module.exports.generate = function(request, reply){
  console.log(request);
  console.log(request.payload) // Show POST parameters

  if (isNaN(request.payload.width) || request.payload.width%2 == 0)
  {
    return reply(Boom.badRequest());
  }

  var len = request.payload.width;
  len < 0 ? (symbA = "+", symbB = ".", len*=-1) : (symbA = ".", symbB = "+");
  var arr = [];
  for (var i = 0; i < len; i++) {
      arr[i] = [];
      for (var j = 0; j < len; j++) {
          arr[i][j] = symbA;
      }
  }
  var mid = Math.floor(len / 2);
  for (var i = 0; i < len; i++) {
      var d = Math.abs(i - mid);
      for (var j = d; j < len - d; j++) {
          arr[i][j] = arr[j][i] = symbB;
      }
  }

  var result = ""
  for (var i = 0; i< len; i++){
    for(var j = 0; j < len; j++){
      result+=arr[i][j];
    }
    i != len-1 ? result+="\n" : result+="";
  }

  reply(result);
}
